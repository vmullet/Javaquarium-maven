<%@page import="com.javaquarium.consts.Param"%>
<%@page import="com.javaquarium.consts.Forward"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-logic"
   prefix="logic"%>
<bean:parameter id="my_redirect" name="<%=Param.REDIRECT%>" />
<div class="right">
   <!-- If there's redirect parameter in the request -->
   <logic:present name="my_redirect">
      <a
         href="<%=Forward.GOTO_CHANGE_LOCALE%>?<%=Param.LOCALE_NAME%>=en&redirect=<%=my_redirect%>"><img
         src="./img/blank.gif" class="flag flag-gb right locale" alt=""
         data-toggle='tooltip' data-placement='bottom'
         title="<bean:message
         key='locale.english' />" /></a>
      <a
         href="<%=Forward.GOTO_CHANGE_LOCALE%>?<%=Param.LOCALE_NAME%>=fr&redirect=<%=my_redirect%>"><img
         src="./img/blank.gif" class="flag flag-fr right locale" alt=""
         data-toggle='tooltip' data-placement='bottom'
         title="<bean:message
         key='locale.french' />" /></a>
   </logic:present>
   <!-- If there's no redirect parameter in the request -->
   <logic:notPresent name="my_redirect">
      <a href="<%=Forward.GOTO_CHANGE_LOCALE%>?<%=Param.LOCALE_NAME%>=en"><img
         src="./img/blank.gif" class="flag flag-gb right locale" alt=""
         data-toggle='tooltip' data-placement='bottom'
         title="<bean:message
         key='locale.english' />" /></a>
      <a href="<%=Forward.GOTO_CHANGE_LOCALE%>?<%=Param.LOCALE_NAME%>=fr"><img
         src="./img/blank.gif" class="flag flag-fr right locale" alt=""
         data-toggle='tooltip' data-placement='bottom'
         title="<bean:message
         key='locale.french' />" /></a>
   </logic:notPresent>
</div>