<%@page import="com.javaquarium.consts.Param"%>
<%@page import="com.javaquarium.consts.Forward"%>
<%@page import="com.javaquarium.consts.SessionVar"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@taglib uri="http://jakarta.apache.org/struts/tags-logic"
   prefix="logic"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <!-- Define bean used in the page -->
   <bean:define id="poisson" name="<%=SessionVar.POISSON.toString()%>"
      type="com.javaquarium.beans.data.PoissonVO" />
   <head>
      <jsp:include page="/jsp/parts/header.jsp">
         <jsp:param name="title" value="page.title.detail_poisson" />
         <jsp:param name="title_arg" value="<%=poisson.getEspece()%>" />
      </jsp:include>
   </head>
   <body>
      <div class="container">
         <!-- Locale switcher with flags / Include a redirect parameter for dynamic forward -->
         <jsp:include page="/jsp/parts/locale.jsp">
            <jsp:param name="redirect"
               value="<%=Forward.GOTO_DETAIL_POISSON.toString() + '?' + Param.ESPECE_ID + '=' + poisson.getCode().toString() %>" />
         </jsp:include>
         <h1>
            <bean:message key="detailPoisson.header.title"
               arg0="<%=poisson.getEspece()%>" />
         </h1>
         <img src="./img/details-discuss.png" class="right" />
         <div class="row">
            <a class="btn btn-primary btn-md"
               href="<%=Forward.GOTO_LISTE_ESPECE%>">
               <i
                  class="fa fa-arrow-circle-left" aria-hidden="true"></i> 
               <bean:message
                  key="message.return_my_aquarium" />
            </a>
            <h3>
               <bean:message key="detailPoisson.header.main_info" />
               <i class="fa fa-info-circle" aria-hidden="true"></i>
            </h3>
            <div class="list-group half-width left">
               <a href="#" class="list-group-item active">
                  <h4 class="list-group-item-heading">
                     <bean:message key="detailPoisson.info.name" />
                  </h4>
                  <p class="list-group-item-text">
                     <%=poisson.getEspece()%>
                  </p>
               </a>
               <a href="#" class="list-group-item">
                  <h4 class="list-group-item-heading">
                     <bean:message key="detailPoisson.info.description" />
                  </h4>
                  <p class="list-group-item-text">
                     <%=poisson.getDescription()%>
                  </p>
               </a>
               <a href="#" class="list-group-item">
                  <h4 class="list-group-item-heading">
                     <bean:message key="detailPoisson.info.color" />
                  </h4>
                  <p class="list-group-item-text">
                     <%=poisson.getCouleur()%>
                  </p>
               </a>
               <a href="#" class="list-group-item">
                  <h4 class="list-group-item-heading">
                     <bean:message key="detailPoisson.info.price" />
                  </h4>
                  <p class="list-group-item-text">
                     <%=poisson.getPrix()%>
                  </p>
               </a>
               <a href="#" class="list-group-item">
                  <h4 class="list-group-item-heading">
                     <bean:message key="detailPoisson.info.dimensions" />
                  </h4>
                  <p class="list-group-item-text">
                     <%=poisson.getDimension()%>
                  </p>
               </a>
            </div>
            <logic:iterate name="<%=SessionVar.LIST_QTY_BY_USER.toString()%>"
               id="qtyUser">
               <bean:define id="user" name="qtyUser" property="key"
                  type="com.javaquarium.beans.data.UserVO" />
               <bean:define id="quantity" name="qtyUser" property="value" />
               <!-- Data for chart display -->
               <html:hidden name="editPoissonForm" property="code"
                  value="<%=user.getUsername() + ';' + quantity.toString()%>"
                  styleClass="chart-data" />
            </logic:iterate>
            <div class="detail-chart left">
               <h3>
                  <bean:message key="detailPoisson.header.qty_by_user" />
                  <i class="fa fa-database" aria-hidden="true"></i> <i
                     class="fa fa-share-alt" aria-hidden="true"></i> <i
                     class="fa fa-users" aria-hidden="true"></i>
               </h3>
               <span class="bold" id="total_fish"></span>
            </div>
            <div id="canvas-holder"
               class="canvas-container-detail canvas-left left">
               <div id="canvas-container">
                  <canvas id="fish_chart_detail"></canvas>
               </div>
            </div>
         </div>
      </div>
      <!-- JS Files -->
      <jsp:include page="/jsp/parts/footer.jsp" />
      <script src="./js/lib/Chart.min.js"></script>
      <script src="./js/chart/detailPie.class.js"></script>
      <script src="./js/chart/chartDetail.js"></script>
   </body>
</html>