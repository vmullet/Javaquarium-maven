package com.javaquarium.business;

import java.util.List;

import com.javaquarium.beans.data.PoissonDO;
import com.javaquarium.beans.data.PoissonVO;
import com.javaquarium.exception.ServiceException;

/**
 * Classic service interface related to Poisson
 * 
 * @author Valentin
 *
 */
public interface IPoissonService {

	/**
	 * Classic method to get all PoissonVO
	 * 
	 * @return List of PoissonVO
	 */
	List<PoissonVO> getAll() throws ServiceException;

	/**
	 * Classic method to retrieve a unique PoissonVO with his name
	 * 
	 * @param name
	 *            The name of the PoissonVO to retrieve
	 * @return The PoissonVO retrieved (or null if not)
	 */
	PoissonVO getOneByName(final String name) throws ServiceException;

	/**
	 * Classic method to retrieve a PoissonVO with his id
	 * 
	 * @param especeId
	 *            The id searched
	 * @return The PoissonVO whose this id belongs
	 * @throws ServiceException
	 *             if a DAOException has already been thrown
	 */
	PoissonVO getOneById(final int especeId) throws ServiceException;

	/**
	 * Classic method to add a PoissonVO
	 * 
	 * @param pvo
	 *            The PoissonVO to add
	 */
	void addOne(final PoissonVO pvo) throws ServiceException;

	/**
	 * Classic method to update a PoissonVO
	 * 
	 * @param pvo
	 *            The poissonVO to update
	 * @throws ServiceException
	 *             if a DAOException has been thrown
	 */
	void update(final PoissonVO pvo) throws ServiceException;

	/**
	 * Classic method to remove a PoissonVO
	 * 
	 * @param pvo
	 *            The poissonVO to remove
	 * @throws ServiceException
	 *             if a DAOException has been thrown
	 */
	void removeOne(final PoissonVO pvo) throws ServiceException;

	/**
	 * Classic method to remove a PoissonVO with his id
	 * 
	 * @param especeId
	 *            The id of the poisson to remove
	 * @throws ServiceException
	 *             if a DAOException has been thrown
	 */
	void removeOne(final int especeId) throws ServiceException;

	/**
	 * Method to convert a PoissonDO into a PoissonVO
	 * 
	 * @param pdo
	 *            The poissonDO to convert
	 * @return The poissonVO resulted of this conversion
	 */
	PoissonVO getVOfromDO(final PoissonDO pdo);

	/**
	 * Method to convert a PoissonVO into a PoissonDO
	 * 
	 * @param pvo
	 *            The poissonVO to convert
	 * @return The poissonDO resulted of this conversion
	 */
	PoissonDO getDOfromVO(final PoissonVO pvo);

}
