package com.javaquarium.business;

import java.util.Map;

import com.javaquarium.beans.data.AquariumPoissonDO;
import com.javaquarium.beans.data.AquariumPoissonVO;
import com.javaquarium.beans.data.PoissonVO;
import com.javaquarium.beans.data.UserVO;
import com.javaquarium.exception.ServiceException;

/**
 * Classic service interface related to aquariumPoisson
 * 
 * @author Valentin
 *
 */
public interface IAquariumPoissonService {

	/**
	 * Method to get all the AquariumPoisson of a specific user
	 * 
	 * @param userId
	 *            The id of the user
	 * @return A map with the especeId as the key and the AquariumPoisson as the
	 *         value
	 * @throws ServiceException
	 *             In case of errors
	 */
	Map<Integer, AquariumPoissonVO> getAllAquariumPoisson(final int userId) throws ServiceException;

	/**
	 * Get a specific AquariumPoisson based on the userId and the especeId
	 * 
	 * @param userId
	 *            The id of the user
	 * @param especeId
	 *            The id of the espece
	 * @return An aquariumPoisson
	 * @throws ServiceException
	 *             In case of errors
	 */
	AquariumPoissonVO getAquariumPoisson(final int userId, final int especeId) throws ServiceException;

	/**
	 * Get a specific AquariumPoisson based on the userVO and the PoissonVO
	 * 
	 * @param user
	 *            The user concerned
	 * @param espece
	 *            The espece concerned
	 * @return An aquariumPoisson
	 * @throws ServiceException
	 *             In case of errors
	 */
	AquariumPoissonVO getAquariumPoisson(final UserVO user, final PoissonVO espece) throws ServiceException;

	/**
	 * Get the quantity of aquariumPoisson by User (useful for stats details)
	 * 
	 * @param especeId
	 *            The especeId of the aquariumPoisson
	 * @return A map with the User as a key and the quantity as the value
	 * @throws ServiceException
	 *             In case of errors
	 */
	Map<UserVO, Integer> getQuantityByUser(final int especeId) throws ServiceException;

	/**
	 * Add a new aquariumPoisson to the user aquarium
	 * 
	 * @param userId
	 *            The id of the user
	 * @param especeId
	 *            The id of the espece
	 * @param quantity
	 *            The quantity to add
	 * @throws ServiceException
	 *             In case of errors (the Aquariumpoisson already exists for
	 *             example)
	 */
	void addNewAquariumPoisson(final int userId, final int especeId, final int quantity) throws ServiceException;

	/**
	 * Add a new aquariumPoisson to the user aquarium
	 * 
	 * @param user
	 *            The user concerned
	 * @param espece
	 *            The espece concerned
	 * @param quantity
	 *            The quantity to add
	 * @throws ServiceException
	 *             In case of errors (the Aquariumpoisson already exists for
	 *             example)
	 */
	void addNewAquariumPoisson(final UserVO user, final PoissonVO espece, final int quantity) throws ServiceException;

	/**
	 * Update an aquariumPoisson in the database
	 * 
	 * @param aPvo
	 *            The aquariumPoisson updated
	 * @param uVo
	 *            The user who owns this aquariumPoisson
	 * @throws ServiceException
	 *             In case of errors
	 */
	void updateAquariumPoisson(final AquariumPoissonVO aPvo, UserVO uVo) throws ServiceException;

	/**
	 * Removes a specific aquariumPoisson in the database
	 * 
	 * @param aPvo
	 *            The aquariumPoisson to remove
	 * @param uVo
	 *            The user who owns this aquariumPoisson
	 * @throws ServiceException
	 *             In case of errors
	 */
	void removeAquariumPoisson(final AquariumPoissonVO aPvo, UserVO uVo) throws ServiceException;

	/**
	 * Return the total of aquariumPoisson in the user aquarium
	 * 
	 * @param userId
	 *            The user who owns this aquarium
	 * @return The total of aquariumPoisson in his aquarium
	 * @throws ServiceException
	 *             In case of errors
	 */
	int getTotalAquariumPoisson(final int userId) throws ServiceException;

	/**
	 * Removes all aquariumPoisson in the auqarium (useful to clear the aquarium)
	 * 
	 * @param userId
	 *            The user who owns this aquarium
	 * @throws ServiceException
	 *             In case of errors
	 */
	void removeAllAquariumPoisson(final int userId) throws ServiceException;

	/**
	 * Mapper to convert an aquariumPoissonDO into a VO
	 * 
	 * @param apDO
	 *            The DO object to convert
	 * @return The VO object resulted of the conversion
	 */
	AquariumPoissonVO getVOfromDO(final AquariumPoissonDO apDO);

	/**
	 * Mapper to convert an aquariumPoissonDO into a VO
	 * 
	 * @param apVO
	 *            The VO object to convert
	 * @param uVo
	 *            The user who owns this aquariumPoisson
	 * @return The DO object resulted of the conversion
	 */
	AquariumPoissonDO getDOfromVO(final AquariumPoissonVO apVO, final UserVO uVo);

}
